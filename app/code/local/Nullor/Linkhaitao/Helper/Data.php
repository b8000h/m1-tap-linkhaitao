<?php 
/**
 * 用于后台设置的开关，职责单一明确
 * 其实使用中并不需要这么多设置项，默认全开就好，将选择的权利留给 Facebook 后台就行
 * 
 * 
 */
class Nullor_Linkhaitao_Helper_Data extends Mage_Core_Helper_Abstract
{

    //Config paths
    const MODULE_ENABLED            = 'nullor_linkhaitao/general/enabled';
    const DEBUG                     = 'nullor_linkhaitao/general/debug';
    const MERCHANT_ID               = 'nullor_linkhaitao/general/merchant_id';
    const MERCHANT_NAME             = 'nullor_linkhaitao/general/merchant_name';
    const SALT                      = 'nullor_linkhaitao/general/salt';
    const COOKIE_LIFETIME           = 'nullor_linkhaitao/general/cookie_lifetime';
    const COMMISSION_RATE           = 'nullor_linkhaitao/general/commission_rate';
    
    /**
     * Check if module is enabled and Pixel ID is set
     * @param  [type]     $store
     * @return boolean
     */
	public function isEnabled($store = null)
    {
        $pixelId = $this->getMerchantId($store);
        return $pixelId && Mage::getStoreConfig(self::MODULE_ENABLED, $store);
    }

    public function isDebug($store = null)
    {
        return $this->isEnabled($store) && Mage::getStoreConfig(self::DEBUG, $store);
    }
    /**
     * Get Pixel ID
     * @param  [type]     $store
     * @return [type]
     */
	public function getMerchantId($store = null)
    {
        return Mage::getStoreConfig(self::MERCHANT_ID, $store);
    }

    public function getMerchantName($store = null)
    {
        return Mage::getStoreConfig(self::MERCHANT_NAME, $store);
    }

    public function getSalt($store = null)
    {
        return Mage::getStoreConfig(self::SALT, $store);
    }

    public function getCookieLifetime($store = null)
    {
        return Mage::getStoreConfig(self::COOKIE_LIFETIME, $store);
    }

    public function getCommissionRate($store = null)
    {
        return 0.01 * Mage::getStoreConfig(self::COMMISSION_RATE, $store);
    }
}