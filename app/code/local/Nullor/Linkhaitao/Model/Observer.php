<?php

class Nullor_Linkhaitao_Model_Observer
{

    public function reportPurchase($observer){

        if (Mage::getSingleton('core/cookie')->get('linkhaitao')){
            //$lastOrderId = Mage::getSingleton('checkout/session')->getLastOrderId();
            //$order = Mage::getSingleton('sales/order')->load($lastOrderId);
            $order = $observer->getOrder();
            $numItems = $order->getAllVisibleItems();

            $itemSkus = array();
            $itemQtys = array();
            $itemAmounts = array();
            $itemCommissions = array();

            if (!Mage::helper('nullor_linkhaitao')->getCommissionRate()){
                $commissionRate = 0.08; 
            } else {
                $commissionRate = Mage::helper('nullor_linkhaitao')->getCommissionRate(); 
            }

            foreach ($numItems as $item) {
                $itemSkus[] = htmlspecialchars($item->getSku());
                $itemQtys[] = $item->getQtyOrdered(); /* getQty() 不行 */
                //$itemAmounts[] = number_format($item->getPrice() * $item->getQtyOrdered(), 2); /* getFinalPrice() 不行 */
                //$itemCommissions[] = number_format($item->getPrice() * $item->getQtyOrdered() * $commissionRate, 2);

                // getPrice() 获取的是当前 currency 的标价，getBasePrice() 获取的是以 base currency 的标价
                $itemAmounts[] = round($item->getBasePrice() * $item->getQtyOrdered(), 2); /* getFinalPrice() 不行 */
                $itemCommissions[] = round($item->getBasePrice() * $item->getQtyOrdered() * $commissionRate, 2);
            }

            $skus = implode('||',$itemSkus);
            $qtys = implode('||',$itemQtys);
            $amounts = implode('||',$itemAmounts);
            $commissions = implode('||',$itemCommissions);
            

            $url = 'http://www.linkhaitao.com/api.php?mod=order';
            // The submitted form data, encoded as query-string-style
            // name-value pairs
            // 居然没有 currency 不可思议
            $params = array('c_track' => Mage::helper('nullor_linkhaitao')->getMerchantId(),
                            'm_id' => Mage::helper('nullor_linkhaitao')->getMerchantName(),
                            'order_id' => $order->getIncrementId(), /* getOrderId() 不行 */
                            'p_cd' => $skus, /* 命名为 sku 为宜 */
                            'quantity' => $qtys,
                            'transaction_date' => time($order->getCreatedAt()),
                            'report_date' => time(), /* 向Linkhaitao发送报告时间（unix时间戳） */
                            'c_status' => 2, /* 既然监听的是 order 成功生成页，为何还要 3 中状态？ */
                            'token' => md5($order->getIncrementId() . Mage::helper('nullor_linkhaitao')->getSalt()),
                            /*'special_tag' => '',*/
                            /*'buyer' => '',*/
                            'amount' => $amounts,
                            'cashback' => $commissions);
            $body = http_build_query($params);
            Mage::getSingleton('core/session')->setData('linkhaitao_purchase', $body);
            
            $c = curl_init($url);
            curl_setopt($c, CURLOPT_POST, true);
            curl_setopt($c, CURLOPT_POSTFIELDS, $body);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            $page = curl_exec($c);
            curl_close($c);
        }

        /**
         * Mupubi
         */
        if (Mage::getSingleton('core/cookie')->get('oid')){
            $oid = Mage::getSingleton('core/cookie')->get('oid');
            $rqid = Mage::getSingleton('core/cookie')->get('rqid');

            //$lastOrderId = Mage::getSingleton('checkout/session')->getLastOrderId();
            //$order = Mage::getSingleton('sales/order')->load($lastOrderId);
            $order = $observer->getOrder();
            $numItems = $order->getAllVisibleItems();

            $itemSkus = array();
            $itemQtys = array();
            $itemPrices = array();

            foreach ($numItems as $item) {
                $itemSkus[] = htmlspecialchars($item->getSku());
                $itemQtys[] = $item->getQtyOrdered();
                $itemPrices[] = round($item->getBasePrice(), 2);
            }

            $skus = implode('^', $itemSkus);
            $qtys = implode('^',$itemQtys);
            $prices = implode('^', $itemPrices);


            //$url = 'http://www.linkhaitao.com/api.php?mod=order';
            $url = 'http://tmoki.com/p.ashx?';

            $params = array(
                            'r' => $rqid,  /* - request Id // Get and store the parameter value of rqid in URL.*/
                            'a' => 196,  /* - Advertiser ID * // a=196, Advertiser parameter a, the Value is Fixed */
                            'e' => 196,  /* - Advertiser ID * // e=196, Advertiser Parameter e, the Value is Fixed */
                            'o' => $oid,  /* - offer id * //Please get and store parameter value of oid in URL. Offer id value */
                            'ect' => $order->getSubtotalInclTax(), /* - Order Total * //the amount of order */
                            'p' => $order->getSubtotalInclTax(), /* – Revenue * //Amount of order, Please exchange order value to USD Dollar */
                            't' => $order->getIncrementId(), /* - Order/Transaction ID //Order NO. */
                            /* ecv – Voucher //the coupon code of the order */
                            'eccu' => 'USD', /* - Order Currency //order currency, Please exchange order currency to USD ecld - Line Item Discount */
                            /* reid1 – // System Reserved ID, Get and store the parameter value of reid1 */
                            /* reid2 – // System Reserved ID, Get and store the parameter value of reid2 */
                            /* reid3 - // System Reserved ID, Get and store the parameter value of reid3  */
                            /* reid4 - // System Reserved ID, Get and store the parameter value of reid4  */
                            /* ecd - Order Discount //the discount amount of the order  */
                            /* ect - Order Value Total //Order amount  */
                            /* ecsh - Order Shipping //shipping fee  */
                            'ecco' => "US", /* – Shipping Country */
                            /* ecrg – Shipping Region  */
                            /* If any of the following parameters are used, all of them will be required. */
                            'ecsk' => $skus, /* - Line Item SKU */
                            'ecqu' => $qtys, /* - Line Item Quantity */
                            'ecpr' => $prices /* - Line Item Price Per Item */
            );


            $body = http_build_query($params);
            
            $c = curl_init($url);
            curl_setopt($c, CURLOPT_POST, true);
            curl_setopt($c, CURLOPT_POSTFIELDS, $body);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            $page = curl_exec($c);
            curl_close($c);
        }
    }

}