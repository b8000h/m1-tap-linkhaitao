<?php

class Nullor_Linkhaitao_Block_Code extends Mage_Core_Block_Template {

	/**
	 * Renders pixel code if module is enabled
	 */
	public function _toHtml()
    {
        if (Mage::helper('nullor_linkhaitao')->isEnabled()){
            return parent::_toHtml();
        }
    }

}