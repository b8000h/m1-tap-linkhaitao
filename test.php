<?php

require_once('app/Mage.php'); // 因为种的实例化统一由 Mage 负责，而 App 需要实例化，所以要先含入 Mage.php
Mage::app(); // 相当于 app('default')

$params = Mage::app()->getRequest()->getParams();
$stime = $params['stime'];
$etime = $params['etime'];
if( !($stime && $etime) ) die();

// 多参考 Varien_Data_Collection_Db 的 _getConditionSql()
$order_collection = Mage::getModel('sales/order')->getCollection();
	//->addFieldToFilter('status',array('in'=>array('processing','complete')))
	//->addFieldToFilter('referrer','linkhaitao')
	//->addFieldToFilter('created_at',array('from'=>$stime,'to'=>$etime))
	//->addAttributeToSelect('name');

$order = array('track'=>'lh_5kixcg',
		'transaction_date',
		'order_id',
		'p_cd',
		'quantity',
		'unit_price',
		'rebate'=>0.08,
		'cashback',
		'c_status'=>2);
$orders = array();

foreach( $order_collection as $o ){
	$order['transaction_date'] = time($o['created_at']); // 不 time() 则做 '2013-03-15 00:01:34' 显示
	$order['order_id'] = $o['increment_id'];

	$items = $o->getAllVisibleItems();
	foreach( $items as $item ){
		$order['p_cd'] = htmlspecialchars($item->getSku());
		$order['quantity'] = $item->getQtyOrdered();
		$order['unit_price'] = round( $item->getBasePrice(), 2);
		$order['cashback'] = $order['unit_price'] * $order['quantity'] * $order['rebate'];

		$orders[] = $order;
	}
}


echo Mage::helper('core')->jsonEncode((object)$orders);